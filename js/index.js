$(window).on("load", function(){
  $("#main-page").on("mouseenter", function(){
    $(this).css("animation", "hover-main-page 500ms ease 0s 1 normal none running")
    setTimeout(function(){
      $(this).css("animation", "")
    },
  500)
  })
  $("#main-page").on("mouseleave", function(){
    $(this).css("animation", "unhover-main-page 500ms ease 0s 1 normal none running")
    setTimeout(function(){
      $(this).css("animation", "")
    },
  500)
  })
  $(".about-menu>li").on("click", function(){
    $(".about-menu>li").removeClass("cmd-current");
    $(this).addClass("cmd-current");
  })
  $(".about-menu>li:first-child").addClass("cmd-current")
})
